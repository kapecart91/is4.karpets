﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Identity4.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProtectedController : ControllerBase
    {
        [HttpGet]
        public string GetProtectedInfo()
        {
            var collectionClaims = string.Join("", User.Claims.Select(x => x.Type + ":\t" + x.Value + "\n"));

            return "Защищенная информация:\n" + collectionClaims;
        }
    }
}
