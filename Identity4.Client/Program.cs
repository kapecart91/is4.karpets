﻿using IdentityModel.Client;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Identity4.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, error) => true;

            await GetDate(httpClientHandler);

            var httpClient = new HttpClient(httpClientHandler);

            var doc = await httpClient.GetDiscoveryDocumentAsync("https://localhost:5001");
            if (doc.IsError)
            {
                Console.WriteLine(doc.Error);
                Console.ReadKey();
                return;
            }

            var tokenClient = new TokenClient(httpClient, new TokenClientOptions()
            {
                Address = doc.TokenEndpoint,
                ClientId = "m2m.client",
                ClientSecret = "511536EF-F270-4058-80CA-1C89C192F69A"
            });

            var userName = "artyom";

            Console.WriteLine("Получение токена для пользователя: " + userName);
            var tokenResponse = await tokenClient.RequestPasswordTokenAsync(userName, "artyom811235!", "scope1");
            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Токен получен:");
            Console.WriteLine(tokenResponse.AccessToken);
            Console.WriteLine("");

            var accessToken = tokenResponse.AccessToken;

            Console.WriteLine("Выполнение авторизованного запроса с полученным токеном:");
            await GetDate(httpClientHandler, accessToken);

            Console.ReadKey();

        }

        private static async Task GetDate(HttpClientHandler httpClientHandler, string accessToken = null)
        {
            var apiClient = new HttpClient(httpClientHandler);

            if (accessToken != null)
                apiClient.SetBearerToken(accessToken);

            var response = await apiClient.GetAsync("https://localhost:44352/api/Protected");
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Пользователь авторизован.");
           
                Console.WriteLine(await response.Content.ReadAsStringAsync());
            }
            else
            {
                Console.WriteLine("Пользователь не авторизован.");
            }

            Console.WriteLine();
        }

    }
}
